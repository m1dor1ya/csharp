﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace VerificationService
{
    /// <summary>
    /// Verifies if the string representation of number is a valid ISBN-10 or ISBN-13 identification number of book.
    /// </summary>
    public static class IsbnVerifier
    {
        /// <summary>
        /// Verifies if the string representation of number is a valid ISBN-10 or ISBN-13 identification number of book.
        /// </summary>
        /// <param name="isbn">The string representation of book's isbn.</param>
        /// <returns>true if number is a valid ISBN-10 or ISBN-13 identification number of book, false otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown if isbn is null.</exception>
        public static bool IsValid(string isbn)
        {
            if (isbn is null)
            {
                throw new ArgumentNullException();
            }

            Regex ISBN10_REGEX = new Regex(@"\d-\d{3}-\d{5}-\d");
            Regex ISBN13_REGEX = new Regex(@"\d{3}-\d-\d{3}-\d{5}-\d");
            int isbnLength = isbn.Length;
            return isbnLength == 0 ||
                   ISBN10_REGEX.IsMatch(isbn) ||
                   ISBN13_REGEX.IsMatch(isbn);
        }
    }
}

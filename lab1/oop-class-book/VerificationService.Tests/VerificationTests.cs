﻿using System;
using NUnit.Framework;

namespace VerificationService.Tests
{
    [TestFixture]
    public class VerificationTests
    {
        [TestCase("1-111-11111-1")]
        [TestCase("2-123-45678-0")]
        [TestCase("8-123-54851-2")]
        public void IsbnVerifier_isValid_isbn10(string isbn10)
        {
            Assert.IsTrue(IsbnVerifier.IsValid(isbn10));
        }

        [TestCase("512-4-676-89127-0")]
        [TestCase("581-5-123-75907-5")]
        [TestCase("913-1-402-12456-8")]
        public void IsbnVerifier_isValid_isbn13(string isbn13)
        {
            Assert.IsTrue(IsbnVerifier.IsValid(isbn13));
        }

        [TestCase("444-4-312-34534")]
        [TestCase("3133")]
        [TestCase("b-34v-a4444-4")]
        [TestCase("a33-2-e67-33333-3")]
        public void IsbnVerifier_isNotValid(string isbn)
        {
            Assert.IsFalse(IsbnVerifier.IsValid(isbn));
        }

        [TestCase("USD")]
        [TestCase("EUR")]
        public void IsoCurrencyValidator_Tests(string currency)
        {
            Assert.IsTrue(IsoCurrencyValidator.IsValid(currency));
        }

        [TestCase("-+-")]
        [TestCase("ddd")]
        [TestCase("SAC")]
        public void IsoCurrencyValidator_ArgumentException(string currency)
        {
            Assert.Throws<ArgumentException>(() => IsoCurrencyValidator.IsValid(currency), "not ISO format");
        }
    }
}

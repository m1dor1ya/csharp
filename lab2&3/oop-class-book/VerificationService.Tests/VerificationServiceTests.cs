﻿using System;
using NUnit.Framework;

#pragma warning disable CA1707 // Identifiers should not contain underscores
#pragma warning disable SA1600 // Elements should be documented

namespace VerificationService.Tests
{
    [TestFixture]
    public class VerificationTests
    {
        [TestCase("3-111-43244-1")]
        [TestCase("5-322-13261-3")]
        [TestCase("6-323-61666-3")]
        public void IsbnVerifier_isValid_isbn10(string isbn10)
        {
            Assert.IsTrue(IsbnVerifier.IsValid(isbn10));
        }

        [TestCase("622-6-443-25525-1")]
        [TestCase("581-5-123-12444-3")]
        [TestCase("432-1-411-12414-6")]
        public void IsbnVerifier_isValid_isbn13(string isbn13)
        {
            Assert.IsTrue(IsbnVerifier.IsValid(isbn13));
        }

        [TestCase("12341-23-121-23")]
        [TestCase("432-1-234-711537-45")]
        [TestCase("51-52-156-8")]
        [TestCase("1233")]
        [TestCase("g1-34-aagf-13")]
        [TestCase("-5-15-234-22")]
        public void IsbnVerifier_isNotValid(string isbn)
        {
            Assert.IsFalse(IsbnVerifier.IsValid(isbn));
        }

        [TestCase("BYN")]
        [TestCase("RUB")]
        [TestCase("USD")]
        public void IsoCurrencyValidator_Tests(string currency)
        {
            Assert.IsTrue(IsoCurrencyValidator.IsValid(currency));
        }

        [TestCase("ddd")]
        [TestCase("---")]
        [TestCase("adafad")]
        public void IsoCurrencyValidator_ArgumentException(string currency)
        {
            Assert.Throws<ArgumentException>(() => IsoCurrencyValidator.IsValid(currency), "not ISO format");
        }
    }
}
